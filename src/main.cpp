#include "AIS_NB_BC95.h"

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <iostream>
#include <ostream>
#include <istream>
#include <vector>
#include <sstream>

// FIXME: get ip from domain
String serverIP = "223.206.228.206";
String serverPort = "12000"; // Your Server Port
String deviceId1 = "10040";
String deviceId2 = "10033";
// String deviceId1 = "10000";
// String deviceId2 = "10001";

AIS_NB_BC95 AISnb;

DHT_Unified dht(4, DHT11);

const long interval = 5000;  //millisecond
unsigned long previousMillis = 0;

long cnt = 0;
long rcnt = 0;
void setup() { 
  AISnb.debug = true;
  Serial.begin(9600);
 
  AISnb.setupDevice("12001");

  String ip1 = AISnb.getDeviceIP();  
  delay(1000);
  
  pingRESP pingR = AISnb.pingIP(serverIP);
  previousMillis = millis();

  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);

  UDPSend udp = AISnb.sendUDPmsgStr(serverIP, serverPort, deviceId1 + ",update");

  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.println("Temperature");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" *C");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" *C");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" *C");  
  Serial.println("------------------------------------");
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.println("Humidity");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println("%");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println("%");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println("%");  
  Serial.println("------------------------------------");

}
std::vector<std::string> split(const std::string& s, char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter))
  {
    tokens.push_back(token);
  }
  return tokens;
}
std::string bufferToString(char* buffer, int bufflen)
{
  std::string ret(buffer, bufflen);

  return ret;
}
void loop() { 
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    cnt++;

    // Get temperature event and print its value.
    sensors_event_t event;  
    dht.temperature().getEvent(&event);
    if (isnan(event.temperature)) {
      Serial.println("Error reading temperature!");
    }
    else {
      Serial.print("Temperature: ");
      Serial.print(event.temperature);
      Serial.println(" *C");
    }
    // Get humidity event and print its value.
    // dht.humidity().getEvent(&event);
    // if (isnan(event.relative_humidity)) {
    //   Serial.println("Error reading humidity!");
    // }
    // else {
    //   Serial.print("Humidity: ");
    //   Serial.print(event.relative_humidity);
    //   Serial.println("%");
    // }
    AISnb.getSignal();
    String udpData = deviceId2 + ",t=" + event.temperature;
    UDPSend udp = AISnb.sendUDPmsgStr(serverIP, serverPort, udpData);
    Serial.print("# UDP Data: ");
    Serial.println(udpData);
    previousMillis = currentMillis;
  }
  UDPReceive resp = AISnb.waitResponse();
  if (resp.data != "") {
    rcnt++;
    Serial.print("# check error (receive/send): ");
    Serial.print(rcnt);
    Serial.print("/");
    Serial.println(cnt);
    std::string hex(resp.data.c_str());
    int len = hex.length();
    std::string data;
    for(int i=0; i< len; i+=2) {
      std::string byte = hex.substr(i,2);
      char chr = (char) (int)strtol(byte.c_str(), NULL, 16);
      data.push_back(chr);
    }
    std::vector<std::string> results = split(data, ',');
    if (results.size() > 2) {
      String devieId = results[0].c_str();
      String action = results[1].c_str();
      String value = results[2].c_str();
      Serial.print("Set deviceID: ");
      Serial.println(devieId);
      Serial.print("Action: ");
      Serial.println(action);
      Serial.print("Value: ");
      Serial.println(value);
      if (devieId == "10040") {
        if (action == "set") {
          if (value == "ON") {
            digitalWrite(2, LOW);
          } else {
            digitalWrite(2, HIGH);
          }
        }
      }
      
    }
    
  }
}